import React, { useContext, useState, useEffect } from "react";
import { AppContext } from "../AppContextProvider";
import { getUserlist } from "../service/api";

export default function Chat() {
  const { username, socketClient } = useContext(AppContext);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    // Fetch user list and update state
    const fetchUserList = async () => {
      try {
        const res = await getUserlist();
        setUsers(Array.from(res.data));
        console.log(users);
      } catch (error) {
        console.error("Error fetching user list:", error);
      }
    };

    fetchUserList();

    if (socketClient) {
      socketClient.emit("login", { name: username });
    }
  }, [socketClient, username]);

  // message
  const [msg, setMsg] = useState();
  // send message
  const handleSubmit = () => {};

  return (
    <>
      {/* user list */}
      <div id="userlist">
        {users.length != 0 ? (
          <ul>
            {users.map((i) => (
              <li key={i._id}>{i.username}</li>
            ))}
          </ul>
        ) : (
          "no users yet"
        )}
      </div>
      {/* send message */}
      <form onSubmit={handleSubmit}>
        <input value={msg} onChange={(e) => setMsg(e.target.value)}></input>
        <button type="submit">Send</button>
      </form>
    </>
  );
}
