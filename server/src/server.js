import * as dotenv from "dotenv";
dotenv.config();

import express from "express";
import path from "path";
import mongoose from "mongoose";
import cors from "cors";
import routes from "./routes/index.js";
import http from "http";

import { Server } from "socket.io";

const app = express();
const port = process.env.PORT ?? 3000;

app.use(cors());

app.use(express.json());

app.use("/", routes);

const __dirname = path.resolve();
app.use(express.static(path.join(__dirname, "../")));
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "../web/dist/index.html"));
});

const server = http.Server(app);
const io = new Server(server, { cors: { origin: "*" } });

const users = [];

io.on("connection", (socket) => {
  socket.on("login", (user) => {
    const newUser = {
      ...user,
      online: true,
      socketId: socket.id,
      messages: [],
    };

    const existingUser = users.find((x) => x.name === newUser.name);
    if (existingUser) {
      existingUser.socketId = newUser.socketId;
      existingUser.online = newUser.online;
    } else {
      users.push(newUser);
    }
    console.log(`${user.name} is online`);
  });

  socket.on("disconnect", (reason) => {
    const user = users.find((x) => x.socketId === socket.id);
    if (user) {
      user.online = false;
      console.log(`${user.name} went offline`);
    }
    if (reason === "io server disconnect") {
      socket.connect();
    }
  });
});

mongoose
  .connect(process.env.DB_URL, { useNewUrlParser: true })
  .then(() =>
    server.listen(port, () => {
      console.log(`App server listening on port ${port}!`);
    })
  )
  .catch((e) => console.log(e));
